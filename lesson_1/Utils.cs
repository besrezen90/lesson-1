﻿using System;


namespace lesson_1
{
    internal class Utils
    {
        public static void Print(string msg) { Console.WriteLine(msg); }

        public static void Pause() { Console.ReadLine(); }

        public static double GetDistance(int x1, int y1, int x2, int y2)
        {
            return Math.Round(Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2)), 2);
        }

    }
}

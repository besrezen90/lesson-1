﻿using System;
using static lesson_1.Utils;

namespace lesson_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Task1();
            Task_2();
            Task_3();
            Task_4();
        }


        #region Task_1

        static void Task1()
        {
            Print("Анкета!");
            Print("Ввдедите свое имя: ");
            string firstName = Console.ReadLine();
            Print("Введите свою фамилию: ");
            string lastName = Console.ReadLine();
            Print("Введите свой возраст: ");
            byte age = byte.Parse(Console.ReadLine());
            Print("Введите свой вес: ");
            float weight = float.Parse(Console.ReadLine());

            Print($"Приветствую, {firstName} {lastName}! Ваш возраст: {age}, ваш вес: {weight}.");
            Pause();
        }

        #endregion

        #region Task_2

        static void Task_2()
        {
            Print("Расчет индекса массы тела");
            Print("Ввдетие вес в килограммах:");
            double weight = Convert.ToDouble(Console.ReadLine());

            Print("Введите рост в метрах:");
            double height = Convert.ToDouble(Console.ReadLine());

            Print($"Ваш ИМТ составляет: {weight / Math.Pow(height, 2)}");
            Pause();
        }

        #endregion

        #region Task_3

        static void Task_3()
        {
            Print("Расчет расстояние между точками");
            Print("Ввдетие x1:");
            int x1 = Convert.ToInt32(Console.ReadLine());

            Print("Ввдетие y1:");
            int y1 = Convert.ToInt32(Console.ReadLine());

            Print("Ввдетие x2:");
            int x2 = Convert.ToInt32(Console.ReadLine());

            Print("Ввдетие y2:");
            int y2 = Convert.ToInt32(Console.ReadLine());

            Print($"Результат вычислений: {GetDistance(x1, y1, x2, y2)}");

            Pause();
        }


        #endregion

        #region Task_4

        static void Task_4()
        {
            string firstValue = "firstValue";
            string secondValue = "secondValue";

            Print($"firstValue = {firstValue}; secondValue = {secondValue}");

            string thirdValue = firstValue;
            firstValue = secondValue;
            secondValue = thirdValue;

            Print($"firstValue = {firstValue}; secondValue = {secondValue}");
            Pause();
        }


        #endregion
    }
}
